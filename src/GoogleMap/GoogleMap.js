import React, { Component } from 'react';
import { View, Image, Text } from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

class MapComponent extends Component {
  constructor(props) {
    super(props);
}


 
render() {
  return(
    <View style={{ paddingTop: 20, flex: 1 }}>
      <GooglePlacesAutocomplete
    placeholder='Enter Location'
      minLength={2} // minimum length of text to search
      autoFocus={false}
      returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
      listViewDisplayed='false'    // true/false/undefined
      fetchDetails={true}
      onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
        console.log('DDATA',data);
        this.props.onSelectLoc(data)
        // this.props.onSelectLoc(location.place_id)
        // console.log('DDATA',data);
      }}
      getDefaultValue={() => {
        if(this.props.location!==null && this.props.location !== undefined){
          return this.props.location;
        } // text input default value
        return '';
      }}
      query={{
        // available options: https://developers.google.com/places/web-service/autocomplete
        key: 'AIzaSyDBDBOnX3yi1xT8UFDYQ6i7kDTU4PkYOXU',
        language: 'en', // language of the results
        // types: '(geocode)' // default: 'geocode'
      }}
      
      styles={{
        containerTop: {
          position: 'absolute',
          top: 0,
          left: 0,
          right: 0,
          alignItems: 'center',
          justifyContent: 'flex-start',
        },
        horizontal: {
          flexDirection: 'row',
          justifyContent: 'space-around',
        },
      //   padding: { padding: 20 },
        listView: {
      // This is the fix:
          backgroundColor: '#ffffff',
        },
        textInputContainer: {
          backgroundColor: 'rgba(0,0,0,0)',
          borderTopWidth: 0,
          borderBottomWidth:0
        },
        description: {
          fontWeight: 'bold',
        },
        predefinedPlacesDescription: {
          color: '#FFF'
        },
        ListView: {
          color: 'rgba(0,0,0,0)',
        }
        
      }}
      
      currentLocation={false}
    />
  </View>
  );
}

}
export default MapComponent;
