import React, { Component } from 'react'
import { Text, View, SafeAreaView, Modal, Alert, ActivityIndicator, AsyncStorage, TextInput, TouchableOpacity } from 'react-native'
import Styles from '../../styleSheet'
import URL from '../config/env'
const relativeUrl = 'users/reset/password'
const apiUrl = URL.apiBaseUrl + relativeUrl
export default class ChangePassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            oldPassword: '',
            newPassword: '',
            confirmPassword: '',
            token: '',
            isLoading: false
        }

        AsyncStorage.getItem('token').then(token => {
            console.log('token', token)
            this.setState({
                token
            })
        }).catch(err => {
            console.log(err)
            return
        })

    }

    render() {

        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#5e5d5d' }}>
                <View>
                    <View style={{ padding: 50, marginTop: 20 }}>
                        <TextInput
                            placeholder='Old Password'
                            placeholderTextColor="white"
                            onChangeText={(oldPassword) => this.setState({ oldPassword })}
                            secureTextEntry
                            style={{
                                borderBottomColor: 'white',
                                borderBottomWidth: 2,
                                color: 'white'
                            }}
                        />
                        <TextInput
                            placeholder='New Password'
                            placeholderTextColor="white"
                            onChangeText={(newPassword) => this.setState({ newPassword })}
                            secureTextEntry
                            style={{
                                borderBottomColor: 'white',
                                borderBottomWidth: 2,
                                color: 'white'
                            }}
                        />
                        <TextInput
                            placeholder='Confirm Password'
                            placeholderTextColor="white"
                            onChangeText={(confirmPassword) => this.setState({ confirmPassword })}
                            secureTextEntry
                            style={{
                                borderBottomColor: 'white',
                                borderBottomWidth: 2,
                                color: 'white'
                            }}
                        />
                    </View>
                    <View style={{ paddingLeft: 60, paddingRight: 60 }}>
                        <TouchableOpacity style={[Styles.registerBtn, Styles.AJ]} onPress={this.submit}>
                            <Text style={Styles.registerBtnTxt}>SUBMIT</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <Modal visible={this.state.isLoading} transparent={true} >
                    <View style={{
                        flex: 1,
                        justifyContent: 'center',
                    }}>
                        <View style={Styles.ModalView}>
                            <ActivityIndicator
                                animating={this.state.isLoading}
                                // style={Styles.indicator}
                                style={{ justifyContent: 'center' }}
                                size='large'
                                color="#fff"
                            />
                            <View style={Styles.MV2}>
                                <Text style={Styles.MVtext}>Please wait...</Text>
                            </View>
                        </View>
                    </View>
                </Modal>
            </SafeAreaView>
        )
    }
    submit = () => {

        this.state.oldPassword
        this.state.newPassword
        this.state.confirmPassword

        if (this.state.oldPassword === '' || this.state.newPassword === '' || this.state.confirmPassword === '') {
            return alert('All fields are require')
        }
        if (this.state.newPassword !== this.state.confirmPassword) {
            return alert('NewPassword and ConfirmPassword donot match')
        } else {
            this.setState({
                isLoading: true
            })

            let data = {
                newPassword: this.state.newPassword,
                oldPassword: this.state.oldPassword,
            }
            const password = JSON.stringify(data)
            console.log('token', this.state.token)
            fetch(apiUrl, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'x-access-token': this.state.token
                },
                body: password
            }).then(response =>
                response.json()).then(response => {
                    console.log("changePassword", response)
                    if (response.isSuccess == true) {
                        this.setState({
                            isLoading: false
                        })
                        this.successMessage();
                    } else {
                        alert(response.error)
                        this.setState({
                            isLoading: false
                        })
                    }
                })
                .catch(error => {
                    console.log('Errorrrrrr:', error)
                    alert("Please Check your internet Connection ")
                    this.setState({
                        animate: false
                    })
                })
            // this.props.navigation.navigate('Info')
        }
    }
    successMessage = () => {
        Alert.alert('',
            'Password updated successfully',

            [
                { text: "OK", onPress: () => this.props.navigation.navigate('Login') },
            ],
            { cancelable: false },
        );
        // return true;
    }
}
