import React, { Component } from 'react'
import { Text, View, ScrollView, TextInput, TouchableOpacity, StyleSheet, ImageBackground, Image } from 'react-native'
import SocketIOClient from 'socket.io-client';
import { GiftedChat } from 'react-native-gifted-chat';
import { default as UUID } from 'uuid';
import Icon from 'react-native-vector-icons/FontAwesome';
var timeoutVar = null;
export default class Chat extends Component {

    constructor(props) {
        super(props);

        this.state = {
            messages: [],
            // password: '',
            typing: false,
            timestamp: 'not timestamp yet'
        }


        this.onSend = this.onSend.bind(this);
        this.onInputTextChanged = this.onInputTextChanged.bind(this);
        this.socket = this.props.navigation.state.params.socket;
        this.userTo = this.props.navigation.state.params.userInfo;
        this.setRoom(this.userTo, global.user);
        this.onRoomSet();
        this.getOldChats();
        this.onReceiveMessage();
        //this.onSenderTyping();
        console.log('User To', this.userTo);
    }
    componentWillMount() {
        this.onSenderTyping()
    }

    onSend(messages = []) {
        console.log('on Send called');
        console.log('Messages', messages);
        console.log('user to id :: ', this.userTo);
        this.socket.emit('chat-msg', { msg: messages[0].text, msgTo: this.userTo.username, date: Date.now() });
        this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, messages),
        }))
    }


    onReceiveMessage() {
        // following code will register event listener for incoming message 
        console.log('Checking for incoming message');
        this.socket.on('chat-msg', (data) => {
            console.log("incoming message data", data)
            if (data.msgFrom !== global.user.username) {
                let messageID = UUID.v4();
                console.log('message id :: ', messageID);
                const incomingMessage = {
                    _id: messageID,
                    text: data.msg,
                    createdAt: data.date,
                    user: {
                        _id: data.msgFrom,
                    },
                };

                this.setState(previousState => ({
                    messages: GiftedChat.append(previousState.messages, incomingMessage),
                }))
            }

        })
    }

    onInputTextChanged() {
        this.socket.emit('typing');
    }

    renderFooter = () => {

        if (this.state.typing) {
            console.log('user is typing')
            return (<View>
                <Text>Typing...</Text>
            </View>);
        }
        console.log('user is not typing');
        return null;
    }


    onSenderTyping() {
        this.socket.on('typing', (data) => {
            console.log('data', data)
            if (!this.state.typing) {
                this.setState({
                    typing: true
                });
                console.log('Typing set to true', this.state.typing);
            } else {
                console.log('clearing out timer');
                clearTimeout(timeoutVar);
            }
            this.timeoutVar = setTimeout(() => {
                console.log('SetTimeout called');
                this.setState({
                    typing: false
                });
                console.log('state after timeout', this.state.typing);
            }, 2000);
        });
    }

    setRoom(userTo, userFrom) {
        console.log('user to :: ', userTo);
        console.log('user from :: ', userFrom);
        var currentRoom = '';
        var reverseRoom = '';
        if (userTo.username == "Group") {
            currentRoom = "Group-Group";
            reverseRoom = "Group-Group";
        }
        else {
            currentRoom = userFrom.username + "-" + userTo.username;
            reverseRoom = userTo.username + "-" + userFrom.username;
        }
        this.socket.emit('set-room', { name1: currentRoom, name2: reverseRoom });
        console.log('set-room emitted successfully');
    }

    onRoomSet() {
        console.log('inside onRoomSet');
        this.socket.on('set-room', (roomId) => {
            console.log('RoomID :: ', roomId);
            this.socket.emit('old-chats-init', { room: roomId, username: global.user.username, msgCount: 1 });
        });
    }

    getOldChats() {
        console.log('getOldChats');
        this.socket.on('old-chats', (data) => {
            console.log('Old Chats :: ', data);
        });
    }

    hideTyping() {
        this.setState({
            typing: false
        });
        console.log('Hide typing called')
    }
    componentDidMount() {

        // this.setState({
        //     messages: [
        //         {
        //             _id: 1,
        //             text: 'Hello developer',
        //             createdAt: new Date(Date.UTC(2016, 7, 30, 17, 20, 0)),
        //             user: {
        //                 _id: 2,
        //                 name: 'Parul Sharma',
        //                 // avatar: 'https://facebook.github.io/react/img/logo_og.png',
        //             },
        //         },
        //     ],
        // });

        //Get older chats above. Looks like this is not implemented on server side.

        // this.onReceiveMessage();
        // this.onSenderTyping();

    }




    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.header}>
                    <View style={styles.headerContent}>
                        <View style={{ flexDirection: 'row' }}>
                            <ImageBackground source={{ uri: "https://cdn3.iconfinder.com/data/icons/social-messaging-productivity-6/128/profile-male-circle2-512.png" }} style={{ height: 40, width: 40, borderRadius: 50, }}>
                                <Image source={{ uri: this.userTo.profilePic }} style={{ height: 40, width: 40, borderRadius: 100, }} />
                            </ImageBackground>
                            <Text style={styles.name}>{this.userTo.username.slice(0, 1).toUpperCase() + this.userTo.username.slice(1, this.userTo.username.length)}</Text>
                            {/* <Text style={{ marginLeft: '2%', marginTop: '4%', fontSize: 15, fontWeight: '100', color: this.userTo.status == 'Online' ? 'green' : 'grey' }}>{this.userTo.status}</Text> */}
                            <Icon name='circle' size={10} style={{ marginLeft: '3%', marginTop: '4%', color: this.userTo.status == 'Online' ? 'green' : 'grey' }} />
                        </View>

                    </View>
                </View>
                <GiftedChat
                    messages={this.state.messages}
                    onSend={messages => this.onSend(messages)}
                    user={{
                        _id: global.user.username,
                        // avatar: 
                    }}
                    // showUserAvatar={true}
                    showAvatarForEveryMessage={true}
                    alwaysShowSend={true}
                    onInputTextChanged={this.onInputTextChanged}
                    renderFooter={this.renderFooter}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    header: {
        backgroundColor: "#f9a43e",
    },
    headerContent: {
        padding: 10,
        // alignItems: 'center',
        justifyContent: 'flex-start'
    },
    avatar: {
        width: 120,
        height: 120,
        borderRadius: 63,
        borderWidth: 4,
        borderColor: "#FFFFFF",
        marginBottom: 10,
    },

    image: {
        width: 50,
        height: 50,
        borderRadius: 40,
        // borderWidth: 4,
        borderColor: "#FFFFFF",
    },

    name: {
        fontSize: 22,
        color: "#FFFFFF",
        fontWeight: '600',
        marginLeft: '3%',
        marginTop: '1%'
    },

    body: {
        padding: 10,
        backgroundColor: "#FFFFFF",
    },

    box: {
        padding: 5,
        // marginTop: 5,
        // marginBottom: 5,
        backgroundColor: '#FFFFFF',
        flexDirection: 'row',
        shadowColor: 'black',
        shadowOpacity: .2,
        shadowOffset: {
            height: 1,
            width: -2
        },
        elevation: 2
    },

    username: {
        color: "#f9a43e",
        fontSize: 15,
        alignSelf: 'center',
        marginLeft: 10
    }
});



