import React, { Component } from 'react';
import Styles from '../../styleSheet'
import { StyleSheet, Modal, View, Text, FlatList, Image, ScrollView, AsyncStorage, TouchableOpacity, ActivityIndicator } from 'react-native';
import SocketIOClient from 'socket.io-client';
import { ListItem } from 'react-native-elements'
import { NavigationActions, StackActions } from 'react-navigation';
import firebase from 'react-native-firebase';
const chatApiUrl = 'http://93.188.167.68:7100/api/users/createOrGet'
let fToken
export default class ChatList extends Component {
    // class ChatList extends Component {
    constructor(props) {

        super(props);
        // const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
        this.state = {
            data: [],
            isLoading: false,
            token: ''
        };
        this.socket = SocketIOClient('http://93.188.167.68:7100');
        this.getToken()
        // this.getOrCreateUser()
    }
    getToken = async () => {
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        fToken = fcmToken
        this.setState({ token: fcmToken })
        console.log('fcmToken', fToken)
    }
    componentDidMount() {
        this.getOrCreateUser()

        // let username = 'satnam'
        // socket = io('http://93.188.167.68:7100/');
    }
    getOrCreateUser = () => {

        this.setState({
            isLoading: true
        })

        let data = {
            userId: global.user.id,
            username: global.user.username,
            email: global.user.email,
            deviceToken: global.fcmToken,
            role: "nothing"
        }

        const userDetail = JSON.stringify(data)
        console.log('stringify(data)', userDetail)
        fetch(chatApiUrl, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: userDetail
        }).then(response =>
            response.json()).then(response => {
                if (response.isSuccess == true) {
                    console.log("chatApiRes", response)
                    this.socket.on('connect', () => {
                        console.log("socketio chat connected.");
                        this.socket.emit('set-user-data', response.message.username, response.message.userId);
                    })
                    this.socket.on('onlineStack', (userList) => {
                        // console.log("friendlist", userList)
                        let dataList = userList.filter(function (item) {
                            return global.user.username != item.username;
                        })
                        this.setState({
                            data: dataList,
                        })
                    })
                    this.setState({
                        isLoading: true
                    })
                } else {
                    this.setState({
                        isLoading: false
                    })
                    alert(response.error);
                }
            })
            .catch(error => {
                console.log('Errorrrrrr:', error)
                alert("Please Check your internet Connection ")
                this.setState({
                    isLoading: false
                })
            })
    }

    navigateToChat = (item) => {

        let resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: 'ChatList' })
            ],
        });
        this.props.navigation.dispatch(resetAction);
        console.log('Item', item)
        this.props.navigation.navigate('Chat', { userInfo: item, socket: this.socket })

        // console.log('going to chat');
        // this.props.navigation.navigate('Chat', { userInfo: item });
    }
    renderRow = ({ item }) => (
        <TouchableOpacity>
            <ListItem
                roundAvatar
                title={item.username}
                subtitle={item.status}
                leftAvatar={{ source: { uri: item.profilePic } }}
                containerStyle={{ borderBottomWidth: 0 }}
                onPress={() => this.navigateToChat(item)}
            />
        </TouchableOpacity>
    )

    startChat = () => {
        this.props.navigation.navigate('editProfile')
    }

    render() {
        return (
            <View>
                <View style={styles.header}>
                    <View style={styles.headerContent}>
                        <Text style={styles.name}>Friend List</Text>
                    </View>
                </View>
                <ScrollView>
                    <FlatList
                        data={this.state.data}
                        renderItem={this.renderRow}
                        keyExtractor={item => item.userId}
                    />
                </ScrollView>
            </View>
        )
    }

}
const styles = StyleSheet.create({
    header: {
        backgroundColor: "#f9a43e",
    },
    headerContent: {
        padding: 10,
        alignItems: 'center',
    },
    avatar: {
        width: 120,
        height: 120,
        borderRadius: 63,
        borderWidth: 4,
        borderColor: "#FFFFFF",
        marginBottom: 10,
    },

    image: {
        width: 50,
        height: 50,
        borderRadius: 40,
        // borderWidth: 4,
        borderColor: "#FFFFFF",
    },

    name: {
        fontSize: 22,
        color: "#FFFFFF",
        fontWeight: '600',
    },

    body: {
        padding: 10,
        backgroundColor: "#FFFFFF",
    },

    box: {
        padding: 5,
        // marginTop: 5,
        // marginBottom: 5,
        backgroundColor: '#FFFFFF',
        flexDirection: 'row',
        shadowColor: 'black',
        shadowOpacity: .2,
        shadowOffset: {
            height: 1,
            width: -2
        },
        elevation: 2
    },

    username: {
        color: "#f9a43e",
        fontSize: 15,
        alignSelf: 'center',
        marginLeft: 10
    }
});

