import React, { Component } from 'react'
import { Text, View, Modal, ActivityIndicator, SafeAreaView, TouchableOpacity, Image, Alert } from 'react-native'
import Styles from '../../styleSheet'
import { AsyncStorage } from 'react-native';
var STORAGE_KEY = 'token';
export default class DeleteAccount extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
        }
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#5e5d5d' }}>
                <View style={{ padding: 30 }}>
                    <View style={{ marginTop: 100, justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/Group-2.png')} style={{ width: 220, height: 220 }} />
                        <Text style={{
                            color: '#fff',
                            fontSize: 25,
                            textAlign: 'center',
                            marginTop: 30
                        }}>Are You Sure You Want To Delete Your Account?</Text>
                    </View>
                </View>

                <View style={{ paddingLeft: 50, paddingRight: 50 }}>
                    <TouchableOpacity style={[Styles.registerBtn, Styles.AJ]} onPress={this.delete}>
                        <Text style={Styles.registerBtnTxt}>DELETE ACCOUNT</Text>
                    </TouchableOpacity>
                </View>
                <Modal visible={this.state.isLoading} transparent={true} >
                    <View style={{
                        flex: 1,
                        justifyContent: 'center',
                    }}>
                        <View style={Styles.ModalView}>
                            <ActivityIndicator
                                animating={this.state.isLoading}
                                // style={Styles.indicator}
                                style={{ justifyContent: 'center' }}
                                size='large'
                                color="#fff"
                            />
                            <View style={Styles.MV2}>
                                <Text style={Styles.MVtext}>Please wait...</Text>
                            </View>
                        </View>
                    </View>
                </Modal>
            </SafeAreaView>
        )
    }

    delete = () => {
        this.setState({
            isLoading: true
        })
        let url = "http://93.188.167.68:3034/api/users/delete";
        AsyncStorage.getItem(STORAGE_KEY).then(token => {
            fetch(url, {
                method: 'put',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'x-access-token': token
                }
            }).then(response =>
                response.json()).then(response => {
                    console.log("delete", response)
                    if (response.isSuccess === true) {
                        this.setState({
                            isLoading: false
                        })
                        this.goToRegistration()
                    } else {
                        this.setState({
                            isLoading: false
                        });
                        alert('Failed to delete account' + response.error);
                    }
                }).catch(error => {
                    console.log('Errorrrrrr:', error)
                    alert("Please Check your internet Connection ")
                    this.setState({
                        isLoading: false
                    })
                });
        });


        // this.props.navigation.navigate('registeration')
    }

    goToRegistration = () => {
        Alert.alert('',
            'Account deleted successfully',
            [
                {
                    text: "OK",
                    onPress: () => this.props.navigation.navigate('Login')
                },
            ],
            { cancelable: false },
        );

    }
}
