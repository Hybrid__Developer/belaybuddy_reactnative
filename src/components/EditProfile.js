import React, { Component } from 'react'
import { Text, View, ScrollView, TextInput, ActivityIndicator, TouchableOpacity, Button, Picker, Image, ImageBackground, Alert } from 'react-native'
import Styles from '../../styleSheet'
import CheckBox from 'react-native-check-box'
var ImagePicker = require('react-native-image-picker');
import moment from 'moment';
import MapComponent from '../GoogleMap/GoogleMap';
import Permissions from 'react-native-permissions'
import Modal from "react-native-modal";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { AsyncStorage } from 'react-native';
import { NavigationActions, StackActions } from 'react-navigation';
const googleApiKey = 'AIzaSyDBDBOnX3yi1xT8UFDYQ6i7kDTU4PkYOXU';
var STORAGE_KEY = 'token';
import URL from '../config/env'
const apiUrl = URL.apiBaseUrl
class EditProfile extends Component {
    constructor(props) {
        super(props);
        const grade = [];
        for (let i = 12; i < 40; i++) {
            grade.push(i.toString())
        }

        this.state = {
            isModalVisible: false,
            isLoading: false,
            grade,
            country: '',
            currentLocation: {
                name: '',
                latitude: null,
                longitude: null
            },
            loc1: this.getLocationData(global.user !== undefined ? global.user.location1 : null),
            loc2: this.getLocationData(global.user !== undefined ? global.user.location2 : null),
            loc3: this.getLocationData(global.user !== undefined ? global.user.location3 : null),
            text: global.user !== undefined ? global.user.about_me : "",
            Origin: '',
            gradeMin: this.getGradeMin(global.user),
            gradeMax: this.getGradeMax(global.user),
            profilePic: {},
            pic: "",
            languages: [
                { name: "English", value: true },
                { name: "French", value: false },
                { name: "German", value: false },
                { name: "Spanish", value: false },
                { name: "Italian", value: false },
                { name: "Portuguese", value: false },
            ],
            climbingStyles: [
                { name: "Bouldering", value: false },
                { name: "Trad Leading", value: false },
                { name: "Sport Leading", value: false },
                { name: "Sport Seconding", value: false },
                { name: "Top Roping", value: false },
                { name: "Indoor Bouldering", value: false },
                { name: "Trad Seconding", value: false },
                { name: "Multi Pitching", value: false },
                { name: "Indoor Climbing", value: false },
                { name: "Alpine Climbing", value: false }
            ],
            date: moment(new Date()).format("DD-MM-YYYY")
        }
        // console.log('guser',global.user)
        this.populateCheckBoxes(this.state.languages, global.user !== undefined ? global.user.languages : "");
        this.populateCheckBoxes(this.state.climbingStyles, global.user !== undefined ? global.user.climbStyle : "");




    }
    componentDidMount() {

        navigator.geolocation.getCurrentPosition(
            (position) => {
                console.log("position", position);
                this.setState({
                    position: position,
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null,
                });
                this.getLocationDetails(position.coords.latitude, position.coords.longitude)
            },
            (error) => this.setState({ error: error.message }),
            { enableHighAccuracy: false, timeout: 200000, maximumAge: 1000 },
        );
    }

    getLocationDetails(latitude, longitude) {
        var currentLocations = [];
        console.log('inside getLocation Details', latitude, longitude);
        url = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + latitude + ',' + longitude + '&key=' + googleApiKey
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                locations = responseJson.results;
                console.log("currentLocation", locations)
                // console.log(location.results[0].formatted_address)
                // this.setState({ address: location.results[0].formatted_address });
                locations.forEach(location => {
                    console.log('location', location)
                    if (location.types.indexOf('locality') !== -1) {
                        this.setState({ country: location.formatted_address });
                        let currentLocation = {
                            name: location.formatted_address,
                            latitude: location.geometry.location.lat,
                            longitude: location.geometry.location.lng
                        }
                        console.log('currentLalong.............', currentLocation)
                        this.setState({ locationDetail: currentLocation });
                        // this.props.onSelectLoction(currentLocation);
                    }
                    // if (location.types.indexOf("administrative_area_level_1") !== -1) {
                    //   if (this.state.country !== null) {
                    //     this.setState({ country: `${this.state.country},  ${location.long_name}` });
                    //   } else {
                    //     this.setState({ country: `${location.long_name}` });
                    //   }
                    // }

                    // if (location.types.indexOf("country") !== -1) {
                    //   if (this.state.country !== null) {
                    //     this.setState({ country: `${this.state.country},  ${location.long_name}` });
                    //   } else {
                    //     this.setState({ country: `${location.long_name}` });
                    //   }
                    // }

                });
            });
    }
    // onBlur() {
    //     console.log('#####: onBlur');
    // }



    markLanguages = (language) => {
        language.value = !language.value
        this.setState({
            isChecked: language.value
        })
    }

    uploadPic = () => {
        ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 200, maxHeight: 200, noData: true }, res => {
            if (res.didCancel) {
                console.log("User cancelled!");
            } else if (res.error) {
                console.log("Error", res.error);
            } else {
                this.setState({
                    profilePic: res
                });
                this.updateProfilePic()
                // global.Profile = this.state.avatarSource
            }
        });
    };

    updateProfilePic = () => {

        this.setState({
            animate: true
        })
        var formdata = new FormData();
        formdata.append('image', {
            image: this.state.profilePic.uri,
            name: 'my_photo.jpg',
            type: 'image/jpg'
        });
        AsyncStorage.getItem(STORAGE_KEY).then(token => {
            console.log("apiUrl", apiUrl + `uploades?image_for=${'profile'}`)
            fetch(apiUrl + `uploades?image_for=${'profile'}`, {
                method: 'put',
                headers: {
                    Accept: '*/*',
                    'Content-Type': 'multipart/form-data',
                    'x-access-token': token
                },
                body: formdata
            }).then(response => response.json()).then(response => {
                console.log('userProfile:', response)
                if (response.isSuccess) {
                    this.setState({
                        animate: false,
                        pic: response.data.profileUrl,
                    })
                    console.log('detail', this.state)
                } else {
                    alert(response.error)
                }
            }).catch(error => {
                console.log('Errorrrrrr:', error)
                alert(error.message)
                this.setState({
                    animate: false
                })
            })
        })
    }

    // getLocationDetail = (location) => {
    //     console.log('editProfile:location', location)
    //     this.setState({ locationDetail: location });
    // }

    toggleModal = () => {

        this.setState({ isModalVisible: !this.state.isModalVisible });

    };

    render() {

        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#5e5d5d' }}>
                <View style={{ paddingLeft: "10%", paddingRight: 100, paddingTop: '10%' }}>
                    <TouchableOpacity onPress={this.uploadPic}>
                        <ImageBackground source={{ uri: "https://cdn3.iconfinder.com/data/icons/social-messaging-productivity-6/128/profile-male-circle2-512.png" }} style={{ height: 150, width: 150, borderRadius: 100, }}>
                            <Image
                                style={{
                                    width: 150,
                                    height: 150,
                                    borderRadius: 75,
                                    // backgroundColor:'#d3d3d3'
                                }}
                                resizeMode='cover'
                                source={{ uri: this.state.pic || "https://cdn3.iconfinder.com/data/icons/social-messaging-productivity-6/128/profile-male-circle2-512.png" }}
                            />
                        </ImageBackground>
                    </TouchableOpacity>
                </View>
                <View style={{ paddingLeft: 30, paddingRight: 30, }}>

                    <View >
                        <Text style={Styles.editTextField}>What Languages Do You Speak?</Text>
                        <View style={{
                            flexDirection: 'row',
                            flexWrap: 'wrap',
                            flexBasis: '50%',
                            flex: 1,
                        }}>
                            {this.state.languages.map(
                                (item, index) => {
                                    return (
                                        <CheckBox key={index}
                                            style={{
                                                width: 130,
                                                marginBottom: 5
                                            }}
                                            onClick={this.markLanguages.bind(this, item)}
                                            isChecked={item.value}
                                            rightText={item.name}
                                            checkBoxColor="#fff"
                                            rightTextStyle={{ color: "#fff", fontSize: 18, }}
                                        />
                                    )
                                }
                            )}
                        </View>
                    </View>

                    <View>
                        <Text style={{ flex: 1, color: "#fff", fontWeight: '500', marginTop: 20, marginBottom: "5%", fontSize: 18 }}>What Is Your Current Location?</Text>
                        <View>
                            <TextInput style={{
                                borderBottomColor: 'white',
                                borderBottomWidth: 2,
                                color: 'white'
                            }} multiline={true}> {this.state.country} </TextInput>
                        </View>
                        {/* <CurrentLocation onSelectLoction={this.getLocationDetail} /> */}
                        {/* <CurrentLocation /> */}
                    </View>

                    <View>
                        <View>
                            <Text style={Styles.editTextField}>Where Do You Want To Climb?</Text>
                            <View style={{ marginBottom: "5%" }}>
                                <Text style={{ flex: 1, color: "#fff", }}>Location#1</Text>
                                <MapComponent location={this.state.loc1.description} onSelectLoc={(location) => {
                                    // console.log('location1###', location)
                                    let locData = {
                                        description: location.description,
                                        place_id: location.place_id
                                    }

                                    this.setState({
                                        location1: locData
                                    });
                                }} />
                            </View>
                        </View>
                        <View style={{ marginBottom: "5%" }}>
                            <Text style={{ flex: 1, color: "#fff", }}>Location#2</Text>
                            <MapComponent location={this.state.loc2.description} onSelectLoc={(location) => {
                                let locData = {
                                    description: location.description,
                                    place_id: location.place_id
                                }
                                this.setState({
                                    location2: locData

                                });
                            }} />
                        </View>
                        <View>
                            <Text style={{ flex: 1, color: "#fff", }}>Location#3</Text>
                            <MapComponent location={this.state.loc3.description} onSelectLoc={(location) => {
                                let locData = {
                                    description: location.description,
                                    place_id: location.place_id
                                }
                                this.setState({
                                    location3: locData

                                });
                            }} />
                        </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: 'row', }}>
                        <Text style={Styles.editTextField}>What Are Your Climbing Styles?
                        </Text>

                        <Modal isVisible={this.state.isModalVisible}
                            onBackdropPress={this.toggleModal}>
                            <View style={{ flex: 1 }}>
                                <ImageBackground source={require('../../assets/Grades.png')} resizeMode="cover" style={{ width: '100%', height: '100%' }}>
                                    <TouchableOpacity style={[Styles.ListItem,]} onPress={this.toggleModal}>
                                        <FontAwesome name="times" size={30} style={{ marginLeft: "90%", }} />
                                    </TouchableOpacity>
                                </ImageBackground>
                            </View>
                        </Modal>
                    </View>
                    <View>
                        {this.state.climbingStyles.map(
                            (item, index) => {
                                return (
                                    <CheckBox key={index}
                                        style={{
                                            marginBottom: 5
                                        }}
                                        onClick={this.markLanguages.bind(this, item)}
                                        isChecked={item.value}
                                        rightText={item.name}
                                        checkBoxColor="#fff"
                                        rightTextStyle={{ color: "#fff", fontSize: 18, }}
                                    />
                                )
                            }
                        )}
                    </View>
                    <View >
                        <Text style={{ flex: 1, color: "#fff", fontWeight: '500', marginTop: 20, marginBottom: '5%', fontSize: 18 }}>What Grades Do You Enjoy Climbing?</Text>
                        <TouchableOpacity onPress={this.toggleModal} >
                            <Text
                                style={{
                                    color: '#f9a43e', textDecorationLine: 'underline',
                                    fontSize: 10,
                                    paddingTop: 20,
                                    paddingLeft: 1
                                }}
                            >What's this</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                        flexBasis: '50%',
                        flex: 1,
                        marginBottom: '5%'
                    }}>
                        <View style={{ backgroundColor: '#FFF', height: 40, marginRight: 15, width: "47%" }}>
                            <Picker selectedValue={this.state.gradeMin} onValueChange={(itemValue) => {
                                this.setState({
                                    gradeMin: itemValue
                                })
                            }}>
                                <Picker.Item label="Minimum" value="" />
                                {this.state.grade.map((item, key) => {
                                    return <Picker.Item label={item} value={item} key={item} />
                                }
                                )}
                            </Picker>
                        </View>
                        <View style={{ backgroundColor: '#FFF', height: 40, width: "47%" }}>
                            <Picker selectedValue={this.state.gradeMax} onValueChange={(itemValue) => {
                                this.setState({
                                    gradeMax: itemValue
                                })
                            }}>
                                <Picker.Item label="Maximum" value="" />
                                {this.state.grade.map((item, key) => {
                                    return <Picker.Item label={item} value={item} key={item} />
                                }
                                )}
                            </Picker>
                        </View>
                    </View>
                    <View style={{ marginBottom: "5%" }}>
                        <Text style={{ flex: 1, color: "#fff", fontWeight: '500', marginBottom: '5%', fontSize: 18 }}>About Me</Text>
                        <View style={{ backgroundColor: '#FFF', height: 100 }}>
                            <TextInput
                                placeholder="Write Here......."
                                multiline={true}
                                numberOfLines={4}
                                onChangeText={(text) => this.setState({ text })}
                                textAlignVertical="top"
                                value={this.state.text}
                            />
                        </View>
                    </View>

                    <View style={{ paddingLeft: "10%", paddingRight: "10%", marginBottom: "5%" }}>
                        <TouchableOpacity style={[Styles.registerBtn, Styles.AJ]} onPress={this.update}>
                            <Text style={Styles.registerBtnTxt}>NEXT</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <Modal visible={this.state.isLoading} transparent={true} >
                    <View style={{
                        flex: 1,
                        justifyContent: 'center',
                    }}>
                        <View style={Styles.ModalView}>
                            <ActivityIndicator
                                animating={this.state.isLoading}
                                // style={Styles.indicator}
                                style={{ justifyContent: 'center' }}
                                size='large'
                                color="#fff" />
                            <View style={Styles.MV2}>
                                <Text style={Styles.MVtext}>Please wait...</Text>
                            </View>
                        </View>
                    </View>
                </Modal>
            </ScrollView>
        )
    }

    successMessage = () => {
        Alert.alert('Congratulations!',
            'Account updated successfully',

            [
                { text: "OK", onPress: () => this.submit() },
            ],
            { cancelable: false },
        );
        // return true;
    }

    update = () => {
        this.setState({
            isLoading: true
        })

        if ((this.state.gradeMin !== null && this.state.gradeMin !== '')
            && (this.state.gradeMax !== null && this.state.gradeMax !== '')) {
            if (this.state.gradeMax <= this.state.gradeMin) {
                alert("Please enter greater value for gradeMax");
                return;
            }

        }
        let data = {
            languages: this.arrayToData(this.state.languages),
            location: this.state.locationDetail,
            climbStyle: this.arrayToData(this.state.climbingStyles),
            gradeMin: this.state.gradeMin,
            gradeMax: this.state.gradeMax,
            location1: this.state.location1 || global.user.location1,
            location2: this.state.location2 || global.user.location2,
            location3: this.state.location3 || global.user.location3,
            about_me: this.state.text

        }

        const userDetail = JSON.stringify(data)
        console.log("editProfile.....................", data)
        AsyncStorage.getItem(STORAGE_KEY).then(token => {
            fetch(apiUrl + 'users', {
                method: 'PUT',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'x-access-token': token
                },
                body: userDetail
            })
                .then(response =>
                    response.json()).then(response => {
                        console.log("update", response)
                        //alert("Response"+JSON.stringify(response));
                        if (response.isSuccess) {
                            global.user = this.getUser(response.data);
                            this.setState({
                                isLoading: false
                            })
                            this.successMessage();
                        } else {
                            console.log("error", response.error.message)
                            Alert.alert('Erorr', response.error.message)
                            this.setState({
                                isLoading: false
                            })
                        }
                    })
                .catch(error => {
                    console.log('Errorrrrrr:', error)
                    alert("Please Check your internet Connection ")
                    this.setState({
                        animate: false
                    })
                })
        });
    }
    getUser = (data) => {
        if (data) {

            let user = {
                languages: data.languages,

                climbStyle: data.climb_style,
                gradeMin: data.gradeMin,
                gradeMax: data.gradeMax,
                location1: data.location1,
                location2: data.location2,
                location3: data.location3,
                locations: data.location,
                about_me: data.about_me
            }
            return user;
        }

    }
    submit = () => {
        let resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: 'editProfile' })
            ],
        });
        this.props.navigation.dispatch(resetAction);
        this.props.navigation.navigate('TabStack')

    }

    arrayToData = (inputAarray) => {
        let arr = [];
        for (var key in inputAarray) {
            if (inputAarray[key].value === true) {
                arr.push(inputAarray[key].name);
            }
        }
        return arr.toString();
    }

    populateCheckBoxes = (checkBoxArray, inputString) => {
        // let arr = inputString.split(',');
        if (inputString !== null && inputString !== undefined && inputString !== "") {
            console.log("editprofile", inputString);
            let arr = inputString.split(',');

            checkBoxArray.map((item, index) => {
                for (var key in arr) {
                    if (item.name === arr[key]) {
                        item.value = true;
                        break;
                    }
                }
            });
        }

    }

    getGradeMin = (user) => {

        if (user !== undefined && user !== null) {
            return user.gradeMin !== null ? user.gradeMin.toString() : "";
        }
        return "";

    }

    getGradeMax = (user) => {
        if (user !== undefined && user !== null) {
            return user.gradeMax !== null ? user.gradeMax.toString() : "";
        }
        return "";
    }
    getLocationData = (locationString) => {
        console.log("Location String", locationString);
        if (locationString !== null && locationString !== '' && locationString !== undefined) {
            try {
                let locObj = locationString;
                console.log('Converted location desc', locObj.description);
                console.log('Converted location place id', locObj.place_id);
                return locObj;
            } catch (exception) {
                console.log('Exception while parsing location ', exception);
                return null;
            }
        }

        return data = {
            description: '',
            place_id: ''
        }
    }
}


export default EditProfile


