import React, { Component } from 'react'
import { Alert, Text, Modal, ActivityIndicator, AsyncStorage, View, SafeAreaView, TextInput, TouchableOpacity, BackHandler, Image, ImageBackground, Linking } from 'react-native'
import Styles from '../../styleSheet'
import HandleBack from '../HandleBack';
import URL from '../config/env'
import firebase from 'react-native-firebase';
const relativeUrl = 'users/login'
const apiUrl = URL.apiBaseUrl + relativeUrl

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            // email: 'bunty.sss42@gmail.com',
            // password: '123456789',
            // email: 'javascript.mspl@gmail.com',
            // password: '1234567890',
            isLoading: false
        }
    }

    async componentDidMount() {
        this.checkPermission();
        this.createNotificationListeners();

    }

    async checkPermission() {
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            this.getToken();
        } else {
            this.requestPermission();
        }
    }

    async getToken() {
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        if (!fcmToken) {
            fcmToken = await firebase.messaging().getToken();
            if (fcmToken) {
                // user has a device token
                await AsyncStorage.setItem('fcmToken', fcmToken);
            }
        }
        global.fcmToken = fcmToken
        console.log('FCM Token : ', fcmToken);
        this.setState({ fcmToken });
    }

    async requestPermission() {
        try {
            await firebase.messaging().requestPermission();
            // User has authorised
            this.getToken();
        } catch (error) {
            // User has rejected permissions
            console.log('permission rejected');
        }
    }

    async createNotificationListeners() {
        /*
        * Triggered when a particular notification has been received in foreground
        * */
        this.notificationListener = firebase.notifications().onNotification((notification) => {
            console.log('Notification Listener :: ', notification);
            const { title, body } = notification;
            this.showAlert(title, body);
        });
        this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
            console.log('Notification Open Listener :: ', notificationOpen);
            const { title, body } = notificationOpen.notification;
            this.showAlert(title, body);
        });
        const notificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {
            console.log('Notification Open :: ', notificationOpen);
            const { title, body } = notificationOpen.notification;
            // this.showAlert(title, body);
        }
        this.messageListener = firebase.messaging().onMessage((message) => {
            //process data message
            console.log('Message Listener :: ', JSON.stringify(message));
        });
    }


    showAlert(title, body) {
        Alert.alert(
            title, body,
            [
                { text: 'OK', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false },
        );
    }

    onBack = () => {
        Alert.alert(
            "",
            "Do You Want to Quit The App?",
            [
                { text: "No", onPress: () => { }, style: "cancel" },
                { text: "Yes", onPress: () => BackHandler.exitApp() },
            ],
            { cancelable: false },
        );
        return true;

    }
    render() {
        return (
            <HandleBack onBack={this.onBack}>
                <ImageBackground source={require('../../assets/Login-background.png')} resizeMode="cover" style={{ width: '100%', height: '100%' }}>

                    <View style={Styles.containerlogin}>

                        <TextInput
                            placeholder='Enter your Email address'
                            placeholderTextColor="white" value={this.state.email}
                            onChangeText={(email) => this.setState({ email })}
                            style={{
                                width: "80%",
                                padding: 15,
                                marginBottom: 10,
                                borderBottomColor: 'white',
                                borderBottomWidth: 2,
                                color: 'white'
                            }}
                        />
                        <TextInput
                            placeholder='Password'
                            placeholderTextColor="white"
                            onChangeText={(password) => this.setState({ password })}
                            secureTextEntry
                            style={{
                                width: "80%",
                                padding: 15,
                                marginBottom: 10,
                                borderBottomColor: 'white',
                                borderBottomWidth: 2,
                                color: 'white'
                            }}
                        />

                        <TouchableOpacity style={[Styles.LoginBtn, Styles.AJ]} onPress={this.login}>
                            <Text style={Styles.LoginBtnTxt}>SIGN IN</Text>
                        </TouchableOpacity>
                        <View style={{ flexDirection: 'row', margin: 20 }}>
                            <TouchableOpacity style={{ flex: 1 }} onPress={() => this.props.navigation.navigate('ForgotPassword')}>
                                <Text style={{ color: '#fff', textDecorationLine: 'underline' }}>
                                    Forget Password</Text>
                            </TouchableOpacity>
                            <View style={{ flex: 1, justifyContent: 'flex-end', flexDirection: "row" }}>
                                <Text style={{ color: '#fff' }}>
                                    New user?</Text>
                                <TouchableOpacity onPress={this.reg}>
                                    <Text style={{ color: '#fff' }}> Signup</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>

                </ImageBackground>

                <Modal visible={this.state.isLoading} transparent={true} >
                    <View style={{
                        flex: 1,
                        justifyContent: 'center',
                    }}>
                        <View style={Styles.ModalView}>
                            <ActivityIndicator
                                animating={this.state.isLoading}
                                // style={Styles.indicator}
                                style={{ justifyContent: 'center' }}
                                size='large'
                                color="#fff" />
                            <View style={Styles.MV2}>
                                <Text style={Styles.MVtext}>Please wait...</Text>
                            </View>
                        </View>
                    </View>
                </Modal>
            </HandleBack>
        )

    }


    login = () => {

        this.state.email
        this.state.password

        if (this.state.email === '' || this.state.password === '') {
            return alert('All fields are required')
        }
        var pattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (!pattern.test(String(this.state.email).toLowerCase())) {
            return alert("Please Enter Valid Email");
        }
        else {

            this.setState({
                isLoading: true
            })

            let data = {
                email: this.state.email,
                password: this.state.password,

            }

            const userDetail = JSON.stringify(data)

            fetch(apiUrl, {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: userDetail
            }).then(response =>
                response.json()).then(response => {
                    console.log("SignUp", response)
                    if (response.isSuccess == true) {

                        this.setState({
                            isLoading: false
                        })
                        //store the token to update data
                        //make user variable global by adding global infront of it
                        AsyncStorage.setItem('token', response.data.token);
                        console.log('login response :: ', response);
                        global.user = this.getUser(response.data);
                        console.log("editProfile:response", response)
                        if (response.data.isNew === true) {
                            this.props.navigation.navigate('editProfile')
                        } else {
                            if (response.data === '') {
                                alert(response.message);
                            } else {

                                this.props.navigation.navigate('TabStack')
                            }
                        }

                    } else {
                        this.setState({
                            isLoading: false
                        })
                        alert(response.error);
                    }
                })
                .catch(error => {
                    console.log('Errorrrrrr:', error)
                    alert("Please Check your internet Connection ")
                    this.setState({
                        isLoading: false
                    })
                })
        }
    }

    getUser = (data) => {
        if (data) {

            let user = {
                languages: data.languages,
                location: data.location,
                climbStyle: data.climb_style,
                gradeMin: data.gradeMin,
                gradeMax: data.gradeMax,
                location1: data.location1,
                location2: data.location2,
                location3: data.location3,
                locations: data.location,
                about_me: data.about_me,
                username: data.userName,
                email: data.email,
                id: data.id

            }
            return user;
        }
    }
    reg = () => {
        this.props.navigation.navigate('registeration')
    }
}


export default Login