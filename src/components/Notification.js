import React, { Component } from 'react'
import { Alert, Text, Modal, ScrollView, StyleSheet, ActivityIndicator, AsyncStorage, View, SafeAreaView, TextInput, TouchableOpacity, BackHandler, Image, ImageBackground, Linking } from 'react-native'



class Notification extends Component {
    constructor(props) {
        super(props);

    }
    renderScreen() {
        return (
            // console.log('userlist', userlist)
            <ScrollView >
                <View style={styles.container}>
                    <View style={styles.header}>
                        <View style={styles.headerContent}>
                            <Text style={styles.name}>Notification</Text>
                        </View>
                    </View>
                    <View style={styles.body}>
                    </View>
                </View>
            </ScrollView >
        );
    }


    render() {
        // if (this.state.data.length === 0) {
        //     return (
        //         <ActivityIndicator
        //             animating={true}
        //             style={Styles.indicator}
        //             size='large'
        //         />
        //     );
        // } else {

        return this.renderScreen()
        // }
    }


}
const styles = StyleSheet.create({
    header: {
        backgroundColor: "#f9a43e",
    },
    headerContent: {
        padding: 20,
        alignItems: 'center',
    },
    avatar: {
        width: 120,
        height: 120,
        borderRadius: 63,
        borderWidth: 4,
        borderColor: "#FFFFFF",
        marginBottom: 10,
    },

    image: {
        width: 50,
        height: 50,
        borderRadius: 40,
        // borderWidth: 4,
        borderColor: "#FFFFFF",
    },
    name: {
        fontSize: 22,
        color: "#FFFFFF",
        fontWeight: '600',
    },
    body: {
        padding: 10,
        backgroundColor: "#FFFFFF",
    },
    box: {
        padding: 5,
        // marginTop: 5,
        // marginBottom: 5,
        backgroundColor: '#FFFFFF',
        flexDirection: 'row',
        shadowColor: 'black',
        shadowOpacity: .2,
        shadowOffset: {
            height: 1,
            width: -2
        },
        elevation: 2
    },
    username: {
        color: "#f9a43e",
        fontSize: 15,
        alignSelf: 'center',
        marginLeft: 10
    }
});








export default Notification