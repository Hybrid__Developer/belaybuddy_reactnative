import React, { Component } from 'react'
import { Text, Alert, Modal, ActivityIndicator, View, ScrollView, TextInput, AsyncStorage, TouchableOpacity, TouchableHighlight, BackHandler, Picker, Image, ImageBackground, Linking } from 'react-native'
var ImagePicker = require('react-native-image-picker');
import Stars from 'react-native-stars';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Styles from '../../styleSheet';
import { Dropdown } from 'react-native-material-dropdown';
import { NavigationActions, StackActions } from 'react-navigation';
import URL from '../config/env'
const apiUrl = URL.apiBaseUrl
class PartnerList extends Component {
    constructor(props) {
        super(props);
        info = [
            {
                imageUrl: "http://www.bolivianmountainguides.com/wp-content/uploads/2010/08/Edu-Peque%C3%B1o-Alpamayo.jpg",
                name: "Lauran",
                gender: "Male",
                age: "32",
                country: "Spain",
                language: ["English", "French", "Spanish"],
                currentLocation: "mohali, Punjab, India",
                intrestedIn: [
                    {
                        name: "location1", value: "Ambala,Haryana,India"
                    }, {
                        name: "location2", value: "Yammuna nagar,Haryana,India"
                    }, {
                        name: "location3", value: "Delhi,India"
                    }
                ], about: "Lorem ipsum dolor sit amet, consecture adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna alique",
                minGrade: 17,
                maxGrade: 23,
                canDo: ["Trade Seconding", "Sport Leading", "Sport Seconding", "Top Roping"]
            }, {
                imageUrl: "http://www.bolivianmountainguides.com/wp-content/uploads/2014/06/Pico-Austria-Dylan.jpg",
                name: "Liza",
                gender: "Female",
                age: "27",
                country: "Spain",
                language: ["English", "French", "Spanish"],
                currentLocation: "mohali, Punjab, India",
                intrestedIn: [
                    {
                        name: "location1", value: "Ambala,Haryana,India"
                    }, {
                        name: "location2", value: "Yammuna nagar,Haryana,India"
                    }, {
                        name: "location3", value: "Delhi,India"
                    }
                ],
                about: "Lorem ipsum dolor sit amet, consecture adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna alique",
                minGrade: 15,
                maxGrade: 22,
                canDo: ["Trade Seconding", "Sport Leading", "Sport Seconding"]
            }
        ]

        this.state = {
            climbingList: [],
            locations: [],
            visible: false,
            hide: false,
            radius: '50',
            locationDetail: {},
            loc1: '',
            loc2: '',
            loc3: '',
            isLoading: false
        }
        this.getUserDetail()

    }


    toggleMenu = () => {
        if (!this.state.visible) {
            this.setState({ visible: true, hide: true });
        } else {
            this.setState({ visible: false, hide: false });
        }
    }

    closeMenu = () => {
        this.setState({ visible: false });
    }

    render() {
        let settings = [{
            value: 'Change Password',
        }, {
            value: 'Delete Account',
        },
        {
            value: 'Logout',
        }];
        const { visible, hide } = this.state

        return (
            <ImageBackground source={require('../../assets/partnerScreen.png')} resizeMode="cover" style={{ width: '100%', height: '100%' }}>
                <ScrollView style={{ flex: 1, }}>
                    <View style={{ paddingLeft: "14%", paddingTop: '5%' }}>
                        <View style={{ flex: 1, flexDirection: 'row', }}>
                            <Text style={{
                                fontSize: 20,
                                fontWeight: 'bold',
                                color: '#f9a43e',
                            }}>
                                Find Your Climbing Partner{"    "}

                            </Text>
                            <Dropdown
                                visible={visible}
                                onClick={this.toggleMenu}
                                tabIndex="0"
                                trigger={['click']} renderBase={() => (
                                    <TouchableOpacity style={[Styles.ListItem,]}>
                                        <FontAwesome name="cog" color="#FFF" size={30} style={{ marginRight: 25, }} />
                                    </TouchableOpacity>
                                )}
                                rippleOpacity={0.2}
                                data={settings}
                                rippleInsets={{ top: 0, bottom: 0, left: 0, right: 0 }}
                                containerStyle={{ width: 100, height: 40 }}
                                dropdownPosition={1}
                                selectedItemColor='#f9a43e'
                                animationDuration={0}
                                inputContainerStyle={{ borderBottomColor: 'transparent' }}
                                editable={false}
                                pickerStyle={{
                                    width: "50%",
                                    left: null,
                                    right: 0,

                                    marginRight: 8,
                                    marginTop: 24
                                }}

                                onChangeText={(value, index) => {

                                    if (index === 0) {
                                        this.toggleMenu
                                        let resetAction = StackActions.reset({
                                            index: 0,
                                            actions: [
                                                NavigationActions.navigate({ routeName: 'PartnerList' })
                                            ],
                                        });
                                        this.props.navigation.dispatch(resetAction);
                                        this.props.navigation.navigate('changePassword')
                                    }
                                    if (index === 1) {
                                        this.toggleMenu
                                        let resetAction = StackActions.reset({
                                            index: 0,
                                            actions: [
                                                NavigationActions.navigate({ routeName: 'PartnerList' })
                                            ],
                                        });
                                        this.props.navigation.dispatch(resetAction);
                                        this.props.navigation.navigate('deleteAccount')
                                    }
                                    if (index === 2) {
                                        this.toggleMenu
                                        this.logout()
                                    }
                                }}
                            />

                        </View>
                    </View>
                    <View style={{ paddingLeft: "10%", paddingRight: "10%" }}>
                        <View >
                            {/* <View style={{ height: 40,  paddingTop: "7%" }}>
                            <Text style={{ fontSize: 15, color: "white" }}>Location   </Text>
                        </View> */}
                            <View style={{ height: 40, width: "100%", paddingLeft: "10%", }}>
                                <Picker mode='dialog'
                                    placeholder="Location"
                                    style={{ color: "white", }}
                                    // note={false}
                                    selectedValue={this.state.locationDetail}
                                    onValueChange={this.setLoaction}
                                >
                                    {this.state.locations.map((item, key) => {
                                        return (<Picker.Item label={item.description} value={item} key={key} />)

                                    })}
                                </Picker>
                            </View>
                        </View>
                        <View style={{
                            flex: 1, flexDirection: 'row',
                            color: "#f9a43e",
                            marginLeft: "15%",
                            marginBottom: "5%"
                        }}>
                            <View style={{ height: 40, paddingTop: 12 }}>
                                <Text style={{ fontSize: 15, color: "#f9a43e" }}>Radius of</Text>
                            </View>
                            <View style={{ height: 40, width: "50%" }}>
                                <Picker mode='dialog'
                                    placeholder="Radius of"
                                    style={{ color: "#f9a43e", marginLeft: 15, justifyContent: 'center', textAlign: 'center' }}
                                    // note={false}
                                    selectedValue={this.state.radius}
                                    onValueChange={this.setRadius}>
                                    <Picker.Item label="50 km" value="50" />
                                    <Picker.Item label="100 km" value="100" />
                                    <Picker.Item label="150 km" value="150" />
                                    <Picker.Item label="200 km" value="200" />
                                    <Picker.Item label="250 km" value="250" />
                                    <Picker.Item label="300 km" value="300" />
                                </Picker>
                            </View>
                        </View>
                        {!this.state.climbingList.length ? <Text style={{ color: 'black' }}>No climber found With selected location. </Text> : null}
                        {this.state.climbingList.map((item, key) =>
                            <View key={key} style={{ marginTop: "10%", marginBottom: "10%" }}>
                                <TouchableOpacity onPress={() => this.getClimbingProfile(item)}>
                                    <ImageBackground
                                        source={{ uri: item.imageUrl }}
                                        imageStyle={{
                                            borderRadius: 20, borderWidth: 1.5,
                                            borderColor: '#d6d7da',
                                        }}
                                        style={{
                                            height: 200,
                                            width: "100%",
                                            position: 'relative', // because it's parent
                                            top: 2,
                                            left: 2,
                                        }}
                                    >
                                    </ImageBackground>
                                </TouchableOpacity>
                                <View style={{
                                    marginTop: "5%",
                                }}>
                                    <Text
                                        style={{
                                            fontWeight: 'bold',
                                            fontFamily: 'arial',
                                            fontSize: 15,
                                            color: '#fff',
                                        }}
                                    >
                                        {item.name}, {item.gender} {item.age || 21},{item.country}.
                            </Text>
                                </View>
                                <View style={{
                                    flex: 1, flexDirection: 'row',
                                    color: "#f9a43e",
                                    marginTop: "5%",
                                }}>
                                    <Text style={{
                                        fontSize: 15,
                                        color: 'white',
                                        fontWeight: 'bold'
                                    }}>
                                        Rating:{" "}
                                    </Text>
                                    <Stars
                                        half={true}
                                        update={(val) => { this.setState({ stars: 3 }) }}
                                        spacing={4}
                                        starSize={20}
                                        count={3}
                                        fullStar={require('../../assets/starFilled.png')}
                                        emptyStar={require('../../assets/starEmpty.png')}
                                        halfStar={require('../../assets/starHalf.png')}
                                    />
                                    <Text style={{
                                        fontSize: 15,
                                        color: 'white',
                                    }}>
                                        {" "} ( From 8 reviews )
                        </Text>
                                </View>

                                <View style={{
                                    flexDirection: 'row',
                                    marginTop: "5%",
                                    marginBottom: "5%"
                                }}>
                                    <Text style={{
                                        fontSize: 15,
                                        color: 'white',
                                        fontWeight: 'bold'
                                    }}>
                                        Speaks:{" "} </Text>
                                    <Text style={{
                                        fontSize: 15,
                                        color: 'white',
                                    }}>{item.languages}</Text>
                                </View>
                                <View style={{
                                    flexDirection: 'row',
                                    marginBottom: "5%"
                                }}>
                                    <Text style={{
                                        fontSize: 15,
                                        color: 'white',
                                        fontWeight: 'bold'
                                    }}>
                                        Currently In:
                                    </Text>
                                    <Text style={{
                                        fontSize: 15,
                                        color: 'white',

                                    }}>
                                        {item.currentLocation.name}
                                    </Text>
                                </View>
                                <View>
                                    <Text style={{
                                        fontSize: 15,
                                        color: 'white',
                                        fontWeight: 'bold'
                                    }}>
                                        Intrested In Climbing In:
                                </Text>
                                    {item.intrestedIn.map(
                                        (item, index) => {
                                            return <Text key={index} style={{
                                                fontSize: 15,
                                                color: 'white',
                                            }}>{item.value}</Text>
                                        }
                                    )}
                                </View>
                            </View>
                        )}
                    </View>
                </ScrollView>
                <Modal visible={this.state.isLoading} transparent={true} >
                    <View style={{
                        flex: 1,
                        justifyContent: 'center',
                    }}>
                        <View style={Styles.ModalView}>
                            <ActivityIndicator
                                animating={this.state.isLoading}
                                // style={Styles.indicator}
                                style={{ justifyContent: 'center' }}
                                size='large'
                                color="#fff" />
                            <View style={Styles.MV2}>
                                <Text style={Styles.MVtext}>Please wait...</Text>
                            </View>
                        </View>
                    </View>
                </Modal>
            </ImageBackground>
        )
    }

    getUserDetail = () => {

        this.setState({
            isLoading: true
        })

        AsyncStorage.getItem('token').then(token => {
            fetch(apiUrl + 'users/getUser', {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'x-access-token': token
                },
            }).then(response => response.json()).then(response => {
                if (response.isSuccess === true) {
                    console.log('partnerlist', response)
                    this.setState({
                        locations: response.data.location,
                        isLoading: false
                    })
                    // this.getClimbingList()
                } else {
                    console.log('erorr:partnerlist', response)
                    Alert.alert('Erorr', response.error.message)
                }
            }).catch(error => {
                console.log('error:catch:partnerlist', error)
                Alert.alert('Erorr', error)
                this.setState({
                    isLoading: false
                })
            })
        })
    }

    setRadius = (radius) => {
        console.log('radius', radius)
        this.setState({
            radius: radius.toString()
        })
        this.getClimbingList()
    }

    setLoaction = (location) => {
        console.log('location', location)
        this.setState({
            locationDetail: location
        })

        this.getClimbingList()
    }

    getClimbingList = () => {

        console.log('radius', this.state.radius)
        this.setState({
            isLoading: true
        })

        AsyncStorage.getItem('token').then(token => {

            fetch(apiUrl + `users/getByLocation?latitude=${this.state.locationDetail.lat}&longitude=${this.state.locationDetail.lng}&radius=${this.state.radius}`, {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'x-access-token': token
                },
            }).then(response => response.json()).then(response => {
                if (response.isSuccess === true) {

                    console.log('userList', response)
                    this.setState({
                        climbingList: response.data,
                        isLoading: false
                    })
                } else {

                    this.setState({
                        isLoading: false
                    })
                    console.log('tterorr', response)
                    Alert.alert('Erorr', response.error.message)
                }
            }).catch(error => {
                Alert.alert('Erorr', error)
                this.setState({
                    isLoading: false
                })
            })
        })
    }
    getLocationData = (locationString) => {
        console.log("Location String", locationString);
        if (locationString !== null && locationString !== '' && locationString !== undefined) {
            try {
                let locObj = locationString
                console.log('Converted location desc', locObj.description);
                console.log('Converted location place id', locObj.place_id);
                return locObj;
            } catch (exception) {
                console.log('Exception while parsing location ', exception);
                return null;
            }
        }
        return data = {
            description: '',
            place_id: ''
        }
    }

    getClimbingProfile(user) {

        let resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: 'PartnerList' })
            ],
        });
        this.props.navigation.dispatch(resetAction);
        this.props.navigation.navigate('PartnerProfile', {
            image: user.imageUrl,
            name: user.name,
            gender: user.gender,
            age: user.age,
            country: user.country,
            language: user.languages,
            currentLocation: user.currentLocation,
            intrestedIn: user.intrestedIn,
            about: user.about,
            minGrade: user.minGrade,
            maxGrade: user.maxGrade,
            canDo: user.canDo
        })

    }
    logout = () => {
        Alert.alert('',
            'Do you want to logout',
            [
                { text: "No", },
                {
                    text: "Yes", onPress: () => {
                        let resetAction = StackActions.reset({
                            index: 0,
                            actions: [
                                NavigationActions.navigate({ routeName: 'PartnerList' })
                            ],
                        });
                        AsyncStorage.removeItem('token')
                        this.props.navigation.dispatch(resetAction);
                        this.props.navigation.navigate('Login')
                    }
                },
            ],
            { cancelable: false },
        );
        // return true;
    }
    // Settings() {
    //     this.props.navigation.navigate('editProfile')
    // }
}


export default PartnerList