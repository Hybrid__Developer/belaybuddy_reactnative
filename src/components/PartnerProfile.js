import React, { Component } from 'react'
import { Text, View, ScrollView, TextInput, TouchableOpacity, BackHandler, Picker, Image, ImageBackground, Linking } from 'react-native'
var ImagePicker = require('react-native-image-picker');
import Stars from 'react-native-stars';
import Modal from "react-native-modal";
import FontAwesome from 'react-native-vector-icons/FontAwesome';
// import console = require('console');
import Styles from '../../styleSheet';
import BottomNavigation, {
    FullTab
} from 'react-native-material-bottom-navigation'


class PartnerProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            info
        }
    }
    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };
    render() {
        const { navigation } = this.props;
        const name = navigation.getParam('name', 'name');
        const age = navigation.getParam('age', 'age');
        const imageUrl = navigation.getParam('image', 'image');
        const gender = navigation.getParam('gender', 'gender');
        const country = navigation.getParam('country', 'country');
        const language = navigation.getParam('language', 'language');
        const currentLocation = navigation.getParam('currentLocation', 'currentLocation');
        const intrestedIn = navigation.getParam('intrestedIn', 'intrestedIn');
        const about = navigation.getParam('about', 'about');
        console.log("about", about)
        const canDo = navigation.getParam('canDo', 'canDo');
        const minGrade = navigation.getParam('minGrade', '5');
        const maxGrade = navigation.getParam('maxGrade', '8');

        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#5e5d5d' }}>
                <View>
                    <View style={{ marginBottom: "10%" }}>

                        <ImageBackground
                            source={{ uri: imageUrl }}
                            style={{
                                height: 250,
                                width: "100%",
                                position: 'relative', // because it's parent
                                top: 2,
                                left: 2
                            }}
                        >
                        </ImageBackground>

                        <View style={{ paddingLeft: "10%", paddingRight: "10%", paddingTop: "5%" }}>
                            <View style={{
                                marginBottom: "5%"
                            }}>
                                <Text
                                    style={{
                                        fontWeight: 'bold',
                                        fontFamily: 'arial',
                                        fontSize: 15,
                                        color: '#fff',
                                    }}
                                >
                                    {name}, {gender} {age},{country}.</Text></View>
                            <View ><Text style={{
                                fontSize: 15,
                                color: 'white',
                            }}>About Me:{" "}{about}</Text></View>
                            <View style={{
                                flex: 1, flexDirection: 'row',
                                color: "#f9a43e",
                            }}>
                                <Text style={{
                                    fontSize: 15,
                                    color: 'white',
                                }}>
                                    Rating:{" "}
                                </Text>
                                <Stars
                                    half={true}
                                    update={(val) => { this.setState({ stars: 3 }) }}
                                    spacing={4}
                                    starSize={20}
                                    count={3}
                                    fullStar={require('../../assets/starFilled.png')}
                                    emptyStar={require('../../assets/starEmpty.png')}
                                    halfStar={require('../../assets/starHalf.png')}
                                />
                                <Text style={{
                                    fontSize: 15,
                                    color: 'white',
                                }}>
                                    {" "} ( From 8 reviews )
                        </Text>
                            </View>

                            <View style={{
                                marginTop: "5%",
                                marginBottom: "5%"
                            }}>
                                <Text style={{
                                    fontSize: 15,
                                    color: 'white',
                                }}>
                                    Speaks:{" "}

                                    <Text style={{
                                        fontSize: 15,
                                        color: 'white',
                                    }}>{language}</Text>


                                </Text>
                            </View>
                            <View style={{

                                marginBottom: "5%"
                            }}>
                                <Text style={{
                                    fontSize: 16,
                                    color: 'white',
                                }}>
                                    Currently In:  {currentLocation.name}
                                </Text>
                            </View>
                            <View style={{

                                marginBottom: "5%"
                            }}>
                                <Text style={{
                                    fontSize: 16,
                                    color: 'white',
                                }}>
                                    Intrested In Climbing:
                                </Text>
                                {intrestedIn.map(
                                    (item, index) => {
                                        return <Text key={index} style={{
                                            fontSize: 15,
                                            color: 'white',
                                        }}>{item.value}</Text>
                                    }
                                )}
                            </View>
                            <View style={{

                                marginBottom: "5%"
                            }}>
                                <Text style={{
                                    fontSize: 16,
                                    color: 'white',
                                }}>
                                    Able to do:
                                </Text>
                                <Text style={{
                                    fontSize: 15,
                                    color: 'white',
                                }}>{canDo}</Text>

                            </View>
                            <View style={{
                                flex: 1, flexDirection: 'row',
                                // marginBottom: "5%"
                            }}>
                                <Text style={{
                                    fontSize: 16,
                                    color: 'white',
                                }}>
                                    Enjoys Climbing Grades: {"\n"}{minGrade}-{maxGrade}
                                </Text>
                                <TouchableOpacity onPress={this.toggleModal} >
                                    <Text
                                        style={{
                                            color: '#f9a43e', textDecorationLine: 'underline',
                                            fontSize: 10,
                                            paddingTop: 20,
                                            paddingLeft: 1
                                        }}
                                    >What's this</Text>
                                </TouchableOpacity>
                                <Modal isVisible={this.state.isModalVisible}
                                    onBackdropPress={this.toggleModal}>
                                    <View style={{ flex: 1 }}>
                                        <ImageBackground source={require('../../assets/Grades.png')} resizeMode="cover" style={{ width: '100%', height: '100%' }}>
                                            <TouchableOpacity style={[Styles.ListItem,]} onPress={this.toggleModal}>
                                                <FontAwesome name="times" size={30} style={{ marginLeft: "90%", }} />
                                            </TouchableOpacity>
                                        </ImageBackground>
                                    </View>
                                </Modal>
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
        )
    }
    image() {

    }
    submit = () => {
    }
    // this.props.navigation.navigate('rate')
}


export default PartnerProfile