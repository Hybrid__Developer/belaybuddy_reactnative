import React, { Component } from 'react'
import DatePicker from 'react-native-datepicker'
import { Text, View, SafeAreaView, Picker, TextInput, TouchableOpacity, BackHandler, Image, ImageBackground, Linking } from 'react-native'
import Styles from '../../styleSheet'
import Stars from 'react-native-stars';
import moment from 'moment';

class Rating extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Username: '',
            email: '',
            password: '',
            gender: '',
            Origin: '',
            date: moment(new Date()).format("DD-MM-YYYY")
        }
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#5e5d5d' }}>
                <View style={{ padding: 35 }}>
                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Image source={require('../../assets/Layer-3.png')} style={{ width: 200, height: 350 }} />
                        <Text style={{
                            color: '#fff',
                            fontSize: 25,
                            textAlign: 'center',
                            marginTop: 30
                        }}>How Would You Rate Your App Expeirence?</Text>
                    </View>
                </View>

                <View style={{ alignItems: 'center' }}>
                    <Stars
                        half={true}
                        update={(val) => { this.setState({ stars: val }) }}
                        spacing={4}
                        starSize={50}
                        count={5}
                        fullStar={require('../../assets/starFilled.png')}
                        emptyStar={require('../../assets/starEmpty.png')}
                        halfStar={require('../../assets/starHalf.png')}
                    />
                </View>

            </SafeAreaView >
        )
    }
    create = () => {
        this.state.Origin,
            this.state.Username,
            this.state.date,
            this.state.email,
            this.state.gender,
            this.state.password
        if (this.state.Origin === '' || this.state.Username === '' || this.state.date === '' || this.state.email === '' || this.state.gender === '' || this.state.password === '') {
            return alert('All fields are require')
        }
        var pattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (!pattern.test(String(this.state.email).toLowerCase())) {
            return alert("Please Enter Valid Email")
        }
    }

    login = () => {

        this.props.navigation.navigate('Login')
    }
}


export default Rating