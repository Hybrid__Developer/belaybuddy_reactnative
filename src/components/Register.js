import React, { Component } from 'react'
import DatePicker from 'react-native-datepicker'
import { Text, View, Modal, ScrollView, NativeModules, Platform, Picker, TextInput, TouchableOpacity, Alert, Image, ImageBackground, Linking, ActivityIndicator } from 'react-native'
import Styles from '../../styleSheet'
import moment from 'moment';
import URL from '../config/env'

const apiUrl = URL.apiBaseUrl

class Register extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            username: '',
            email: '',
            password: '',
            gender: '',
            country: '',
            countries: [],
            date: moment(new Date()).format("DD-MM-YYYY"),
            isLoading: false
        }
        this.getCountriesList()
        Alert.alert('Info!', 'Ensure your details are correct because only your password can be changed later',
            [
                { text: "OK" },
            ],
            { cancelable: false },
        );

    }


    render() {
        // alert('rendering'+JSON.stringify(this.state.countries));
        // if (this.state.isLoading) {
        //     return (
        //       <View style={{flex: 1, paddingTop: 20}}>
        //         <ActivityIndicator />
        //       </View>
        //     );
        //   }
        return (
            <ScrollView style={{ flex: 1, backgroundColor: '#5e5d5d' }}>
                {/* // <SafeAreaView style={{flex:1, backgroundColor: '#5e5d5d' }}> */}
                <View style={{ marginTop: 40, justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('../../assets/Belay-Buddy-logo.png')} style={{ width: 170, height: 120 }} />
                </View>
                <View style={{ paddingLeft: 50, paddingRight: 50 }}>
                    <TextInput
                        placeholder='Username'
                        placeholderTextColor="white"
                        onChangeText={(username) => this.setState({ username })}
                        style={{
                            borderBottomColor: 'white',
                            borderBottomWidth: 2,
                            color: 'white'
                            // marginBottom: 30,
                        }}
                    />
                    <TextInput
                        placeholder='Email'
                        placeholderTextColor="white"
                        onChangeText={(email) => this.setState({ email })}
                        style={{
                            borderBottomColor: 'white',
                            borderBottomWidth: 2,
                            color: 'white'
                        }}
                    />
                    <TextInput
                        placeholder='Password'
                        placeholderTextColor="white"
                        onChangeText={(password) => this.setState({ password })}
                        secureTextEntry
                        style={{
                            borderBottomColor: 'white',
                            borderBottomWidth: 2,
                            color: 'white',
                            marginBottom: 20
                        }}
                    />
                    <View style={{ backgroundColor: '#FFF', marginBottom: 20, height: 40, }}>
                        <Picker mode='dialog'
                            placeholder="Select Gender"
                            placeholderStyle={{ color: "white" }}
                            // note={false}
                            selectedValue={this.state.gender}
                            onValueChange={(itemValue, itemIndex) =>
                                this.setState({ gender: itemValue })
                            }>
                            <Picker.Item label="Select Gender" value="" />
                            <Picker.Item label="Male" value="male" />
                            <Picker.Item label="Female" value="female" />
                        </Picker>
                    </View>
                    <View style={Styles.root}>
                        <View style={Styles.rowContainer}>
                            <Text style={Styles.text}>DOB</Text><DatePicker
                                style={{ width: 200 }}
                                date={this.state.date}
                                mode="date"
                                placeholder="select date"
                                format="DD-MM-YYYY"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                customStyles={{
                                    dateInput: {

                                        backgroundColor: 'white'
                                    }
                                }}
                                onDateChange={(date) => { this.setState({ date: date }) }}
                            />
                        </View>
                    </View>
                    <View style={{ backgroundColor: '#FFF', height: 40, marginBottom: 20 }}>
                        <Picker mode='dialog'
                            placeholder="Country of Origin"
                            placeholderStyle={{ color: "white" }}
                            // note={false}
                            selectedValue={this.state.country}
                            onValueChange={(itemValue, itemIndex) =>
                                this.setState({ country: itemValue })
                            }>
                            {this.state.countries.map((item, key) => {
                                return <Picker.Item label={item.country_name} value={item.country_name} key={key} />
                            })
                            }
                        </Picker>
                    </View>
                    <TouchableOpacity style={[Styles.registerBtn, Styles.AJ]} onPress={this.create}>
                        <Text style={Styles.registerBtnTxt}>CREATE ACCOUNT</Text>
                    </TouchableOpacity>
                    <View style={Styles.container}>
                        <TouchableOpacity onPress={this.login}>
                            <Text style={{
                                color: 'white',
                            }}>Already have an account?
                                <Text style={{
                                    textDecorationLine: 'underline',
                                    color: '#f9a43e',
                                }}> Sign in</Text>
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <Modal visible={this.state.isLoading} transparent={true} >
                    <View style={{
                        flex: 1,
                        justifyContent: 'center',
                    }}>
                        <View style={Styles.ModalView}>
                            <ActivityIndicator
                                animating={this.state.isLoading}
                                // style={Styles.indicator}
                                style={{ justifyContent: 'center' }}
                                size='large'
                                color="#fff"
                            />
                            <View style={Styles.MV2}>
                                <Text style={Styles.MVtext}>Please wait...</Text>
                            </View>
                        </View>
                    </View>
                </Modal>
                {/* // </SafeAreaView> */}
            </ScrollView>
        )
    }
    SuccessMessage = () => {


        Alert.alert('Congratulations!',
            'Account created successfully. Please check your email for verification',
            [
                { text: "OK", onPress: () => this.login() },
            ],
            { cancelable: false },
        );
        // return true;
    }
    create = () => {

        this.state.country,
            this.state.username,
            this.state.date,
            this.state.email,
            this.state.gender,
            this.state.password

        if (!this.state.username == '' || !this.state.email == '' || !this.state.password == '' || !this.state.gender == '' || !this.state.Origin === '' || !this.state.date === '') {
            var pattern = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
            var passwordPattern = /.*[0-9]+.*/i
            if (!pattern.test(String(this.state.email).toLowerCase())) {
                return alert("Please Enter Valid Email")
            }

            if (this.state.password.length < 8) {
                return alert("Passwords must be 8 character")
            }
            if (!passwordPattern.test(String(this.state.password))) {
                return alert('Password must contain atleast 1 digit.')
            }
            console.log('user', this.state.username)
            if (this.state.username.length > 15) {
                console.log('user2', this.state.username.length)
                return alert("User name must be of 15  characters maximum")
            }

            this.setState({
                isLoading: true
            })

            let data = {

                userName: this.state.username,
                email: this.state.email,
                password: this.state.password,
                country: this.state.country,
                dob: this.state.date,
                gender: this.state.gender

            }
            const userDetail = JSON.stringify(data)
            // alert(userDetail);
            fetch(apiUrl + 'users/register', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: userDetail
            }).then(response =>
                response.json()).then(response => {
                    console.log("SignUp", response)

                    if (response.isSuccess === true) {
                        this.setState({
                            isLoading: false
                        })
                        this.SuccessMessage()
                    } else {
                        alert(response.error);
                        this.setState({
                            isLoading: false
                        })
                    }
                })
                .catch(error => {
                    console.log('Errorrrrrr:', error)
                    alert("Please Check your internet Connection ")
                    this.setState({
                        animate: false
                    })
                })
        } else {
            alert('All fields are require')
        }
    }

    login = () => {
        this.props.navigation.navigate('Login')
    }

    getCountriesList = () => {
        console.log('getting countries list');
        let countriesUrl = "http://93.188.167.68:3034/api/countries";
        console.log('countries api url', countriesUrl);

        this.setState({
            isLoading: true,

        });
        let countries = [];
        fetch(apiUrl + 'countries')
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.isSuccess) {
                    console.log('received countries');
                    this.setState({
                        isLoading: false,
                        countries: responseJson.data
                    });
                }
            })
            .catch((error) => {
                console.log("error", error);
                alert('Error occurred getting countries');
            });
        // }
        console.log('done fetching countries');
    }
}


export default Register