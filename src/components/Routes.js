
import React, { Component } from 'react'
import { Text, createAppContainer, createStackNavigator, createSwitchNavigator, createBottomTabNavigator } from 'react-navigation';
import LoginScreen from "./Login"
import SplashScreen from "./Splash"
import RegisterScreen from "./Register"
import ChangePasswordScreen from "./ChangePassword"
import DeleteAccountScreen from "./DeleteAccount"
import EditProfileScreen from "./EditProfile"
import ratingScreen from "./Rating"
import PartnerListScreen from "./PartnerList"
import PartnerProfileScreen from "./PartnerProfile"
import TabNavigator from './TabNavigation'
import Chat from './Chat';
import ChatList from './ChatList';

const loginStack = createStackNavigator({
    Splash: {
        screen: SplashScreen,
        navigationOptions: {
            header: null
        }
    },
    Login: {
        screen: LoginScreen,
        navigationOptions: {
            header: null

        }
    },
    registeration: {
        screen: RegisterScreen,
        navigationOptions: {
            header: null

        }
    },
    editProfile: {
        screen: EditProfileScreen,
        navigationOptions: {
            header: null

        }
    },
    rating: {
        screen: ratingScreen,
        navigationOptions: {
            header: null

        }
    },
    changePassword: {
        screen: ChangePasswordScreen,
        navigationOptions: {
            header: null

        }
    },
    deleteAccount: {
        screen: DeleteAccountScreen,
        navigationOptions: {
            header: null

        }
    }, PartnerList: {
        screen: PartnerListScreen,
        navigationOptions: {
            header: null
            // headerStyle: { backgroundColor: 'blue' },
            // title: <Text style={{
            //     color: 'white', fontSize: 15,
            //     fontWeight: 'bold', justifyContent: 'center', alignItems: 'center',
            // }}>Profile</Text>,
        }
    },
    PartnerProfile: {
        screen: PartnerProfileScreen,
        navigationOptions: {
            header: null

        }
    },
    ChatList: {
        screen: ChatList,
        navigationOptions: {
            header: null
        }
    },
    Chat: {
        screen: Chat,
        navigationOptions: {
            header: null
        }
    },

}, {
    }, {
        initialRouteName: 'Splash',
    });

// const AppBottomTabNavigator = createBottomTabNavigator({
//     Home : PartnerListScreen
// }, {
//     navigationOptions : ({navigation}) => ({

//         tabBarIcon: ({ focused, tintColor }) => {
//             console.log(navigation)
//             const { routeName } = navigation.state;
//             let icon;
//             switch(routeName) {
//                case 'Home':
//                   icon = "home";
//                 break;
//               }

//               return <Ionicons 
//                        name={icon} 
//                        size={25} 
//                        color={tintColor} />;
//              },
//     })
// });
// const Routes = createAppContainer(loginStack)
const Routes = createAppContainer(createSwitchNavigator({
    LoginStack: loginStack,
    TabStack: TabNavigator

}))

export default Routes