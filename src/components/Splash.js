import React, { Component } from 'react'
import { Image, View, SafeAreaView, ImageBackground, } from 'react-native'
class Splash extends Component {
    componentDidMount() {
        // Start countithisng when the page is loaded

        this.timeoutHandle = setTimeout(() => {
            this.retrieveData()
        }, 2000);

    }
    retrieveData() {
        this.props.navigation.navigate('Login')
    }
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true
        }
    }
    componentWillUnmount() {
        clearTimeout(this.timeoutHandle); // This is just necessary in the case that the screen is closed before the timeout fires, otherwise it would cause a memory leak that would trigger the transition regardless, breaking the user experience.
    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <ImageBackground source={require('../../assets/splash.png')} resizeMode="cover" style={{ width: '100%', height: '100%' }}>
                    <View style={{ padding: 30 }}>
                        <View style={{ marginTop: "30%", justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={require('../../assets/Belay-Buddy-logo.png')} style={{ width: 160, height: 120 }} />
                        </View>
                    </View>
                </ImageBackground>
            </SafeAreaView>
        )
    }
}

export default Splash