import React from 'react'
import { Image, } from 'react-native'
import { createBottomTabNavigator } from 'react-navigation';
import LoginScreen from "./Login"
import SplashScreen from "./Splash"
import RegisterScreen from "./Register"
import ChangePasswordScreen from "./ChangePassword"
import DeleteAccountScreen from "./DeleteAccount"
import EditProfileScreen from "./EditProfile"
import ratingScreen from "./Rating"
import PartnerListScreen from "./PartnerList"
import PartnerProfileScreen from "./PartnerProfile"
import ChatList from './ChatList'
import Icon from 'react-native-ionicons'
import GradeScreen from './Grade'
import NotificationScreen from './Notification'


var TabNavigator = createBottomTabNavigator({
    tab1: {
        screen: PartnerListScreen,
        navigationOptions: {
            // tabBarLabel: "Home",
            tabBarIcon: ({ tintColor }) => (
                <Icon name="home" size={35} color={'black'} />
            )
        }
    },

    tab2: {
        screen: ChatList,
        navigationOptions: {

            // tabBarLabel: "Home",
            tabBarIcon: ({ tintColor }) => (
                <Icon name="chatboxes" size={35} color={'black'} />
            )
        }
    },

    tab3: {
        screen: GradeScreen,
        navigationOptions: {
            // tabBarLabel: "Home",
            tabBarIcon: ({ tintColor }) => (
                <Image
                    source={require('../../assets/mount1.png')} style={{ width: 30, height: 40 }} >
                </Image>

            )

        }
    }, tab4: {
        screen: NotificationScreen,
        navigationOptions: {
            tabBarLabel: "Home",
            tabBarIcon: ({ tintColor }) => (
                <Icon name="notifications" size={35} color={'black'} />

            )
        }
    }, tab5: {
        screen: EditProfileScreen,
        navigationOptions: {
            // tabBarLabel: "Home",
            tabBarIcon: ({ tintColor }) => (
                <Icon name="person" size={35} color={'black'} />
            )
        }
    },

}, {
        tabBarPosition: 'bottom',
        swipeEnabled: true,
        tabBarOptions: {
            activeTintColor: 'black',
            activeBackgroundColor: '#f9a43e',
            inactiveTintColor: 'black',
            inactiveBackgroundColor: '#faba6b',
            showIcon: true,
            showLabel: false,
            labelStyle: {
                fontSize: 16,

            }
        }
    }
);
// tab1: { screen: PartnerListScreen, },

TabNavigator.navigationOptions = {
    title: "tab example"
};

export default TabNavigator;