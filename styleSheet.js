
import {
    Dimensions,
    StyleSheet,
} from 'react-native';
const window = Dimensions.get('window');

const Styles = StyleSheet.create({
    containerlogin: {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 100
    },
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 10,
    },

    AJ: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    LoginBtn: {
        height: 50,
        backgroundColor: '#f9a43e',
        width: "60%",
        marginTop: 10,
        // elevation: 10,
        // marginLeft: 60,
        // marginRight: 60
    },
    modal: {
        flex: 1,
        alignItems: 'center',
        padding: 100
    },
    ModalView: {
        backgroundColor: '#636FA4',
        height: 80,
        borderRadius: 3,
        flexDirection: 'row',
        paddingLeft: 20,
        marginLeft: 50,
        marginRight: 50,
        elevation: 40,
    },

    LoginBtnTxt: {
        color: '#fff',
        fontSize: 15,
        textAlign: 'center'
    }, ModalView: {
        backgroundColor: '#f9a43e',
        height: 80,
        borderRadius: 3,
        flexDirection: 'row',
        paddingLeft: 20,
        marginLeft: 50,
        marginRight: 50,
        elevation: 40,
    },
    MV2: {
        flex: 1,
        justifyContent: 'center',
        marginLeft: 30,
    },
    MVtext: {
        color: '#fff',
        fontWeight: '500',
    },
    registerBtn: {
        height: 50,
        backgroundColor: '#f9a43e',
        elevation: 10,
        marginLeft: 10,
        marginRight: 10
    },
    registerBtnTxt: {
        color: '#fff',
        fontSize: 15,
        textAlign: 'center'
    },
    contentContainer: {
        paddingVertical: 20
    },
    ProfileDetail: {
        justifyContent: 'center',
        paddingLeft: 10,
        backgroundColor: '#fff',
        height: 60,
        marginBottom: 10,
        elevation: 5,
        borderRadius: 2,
    },
    ProfileHeader: {
        paddingLeft: 20,
        height: 80,
        justifyContent: 'center',
        backgroundColor: '#636FA4'
    },
    ProfileName: {
        fontWeight: '500',
        color: '#fff',
        fontSize: 18,
    },

    root: {
        flex: 1,
        marginBottom: 20,
        flexDirection: "column",
    },
    rowContainer: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },
    text: {
        flex: 1,
        color: '#fff',
    },
    textInput: {
        flex: 1,
        backgroundColor: 'white',
        borderColor: 'black',
    },
    editTextField: {
        flex: 1,
        color: "#fff",
        fontWeight: '500',
        marginTop: 20,
        marginBottom: 10,
        fontSize: 17
    },
    floatingLableInput:
    {
        borderWidth: 0,
        color: '#fff',
        fontSize: 15
    },
    indicator: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }

})
export default Styles